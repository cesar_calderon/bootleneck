from time import time
import warnings
warnings.filterwarnings("ignore")


def bottleneeck_csv(path_to_data, path_to_save):
    '''
     Genera un dataset mas inteligente para el analisis de cuellos de
       botellas
    :bar path_to_data: csv formato tipo felipe
    :bar path_to_save: destino donde se guardaran los csv de colas y
        activities, debe terminar con "/"

    :return csv1, csv2: ademas de guardar los csv generados, esta funcion
        devuelve dos objetos DataFrame de la libreria pandas en orden:
        cv1 = DataFrame(actividades), csv2 = DataFrame(COLAS)
    '''

    import pandas as pd
    origin = pd.read_csv(path_to_data, quotechar="'",
                         engine="python", encoding="utf-8")
    zone_max_number = int(origin.columns[-1].split('(')[-1].split(')')[0])
    data = origin.copy()
    ZO = set()
    for i in range(zone_max_number - 1):
        for zo in data['Zone Name (' + str(i + 1) + ')'].unique():
            if (zo in ZO) or (not zo):
                continue
            else:
                ZO.add(zo)
    ZO = list(ZO)
    # COLAS = None
    # for zo in ZO:
    #     if not zo:
    #         continue
    #     for i in range(zone_max_number - 2):
    #         asd = data[data['Zone Name (' + str(i + 2) + ')'] == zo]
    #         asd = asd[['Cycle Id', 'Origin', 'Destination', 'Product',
    #                    'Zone Name (' + str(i + 2) + ')',
    #                    'Zone End (' + str(i + 1) + ')',
    #                    'Zone Start (' + str(i + 2) + ')']]
    #         z_end = 'Zone End (' + str(i + 1) + ')'
    #         z_start = 'Zone Start (' + str(i + 2) + ')'
    #         asd[z_end] = pd.to_datetime(asd[z_end])
    #         asd[z_start] = pd.to_datetime(asd[z_start])
    #         asd.columns = ['ID', 'Origin', 'Destination', 'Product',
    #                        'ZO', 'Start Cola', 'End Cola']
    #         if isinstance(COLAS, pd.DataFrame):
    #             COLAS = pd.concat([COLAS, asd], axis=0, sort=False)
    #         else:
    #             COLAS = asd

    # for zo in ZO:
    #     if not zo:
    #         continue
    #     asd = data[data['Zone Name (1)'] == zo]
    #     if len(asd) == 0:
    #         continue
    #     asd['Zone Start (1)'] = pd.to_datetime(asd['Zone Start (1)'])
    #     asd['Zone End (0)'] = asd.apply(
    #         lambda asd: asd['Zone Start (1)'] -
    #         pd.Timedelta(asd['Zone Pre Time (1)'], unit='s'), axis=1)

    #     asd = asd[['Cycle Id', 'Origin', 'Destination', 'Product',
    #                'Zone Name (1)', 'Zone End (0)', 'Zone Start (1)']]
    #     asd.columns = ['ID', 'Origin', 'Destination', 'Product', 'ZO',
    #                    'Start Cola', 'End Cola']

    #     COLAS = pd.concat([COLAS, asd], axis=0, sort=False)

    ACTIVIDADES = None

    for zo in ZO:
        if not zo:
            continue
        for i in range(0, zone_max_number - 1):
            asd = data[data['Zone Name (' + str(i + 1) + ')'] == zo]
            asd = asd[['Cycle Id', 'Origin', 'Destination', 'Product',
                       'Zone Name (' + str(i + 1) + ')',
                       'Zone Start (' + str(i + 1) + ')',
                       'Zone End (' + str(i + 1) + ')',
                       'Zone Pre Time (' + str(i + 1) + ')']]
            asd.columns = ['ID', 'Origin', 'Destination', 'Product', 'ZO',
                           'Start Act', 'End Act','Pre Time']
            if isinstance(ACTIVIDADES, pd.DataFrame):
                ACTIVIDADES = pd.concat([ACTIVIDADES, asd], axis=0, sort=False)
            else:
                ACTIVIDADES = asd

    ACTIVIDADES.to_csv(path_to_save + 'actividad_n_v2.csv')
    #COLAS.to_csv(path_to_save + 'colas_n.csv')
    #return ACTIVIDADES, COLAS
    return ACTIVIDADES

# In[]
start = time()
to_save = './'
act, col = bottleneeck_csv('./SQMStats_CS_Bottleneck_20190107-20203106.csv',
                           to_save)
print(time() - start)
