# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import datetime

file = "SQMStats_CS_Bottleneck_20190101-20210101.csv"
data = pd.read_csv(file ,quotechar="'",engine="python",encoding="utf-8")

    #Columnas a eliminar
cols = ['Quarter','Type','Fleet','Asset','Waybill','Product','Total time']
#zone_name = ['Zone Name (%s)'%x for x in range(9,99)]
#zone_start = ['Zone Start (%s)'%x for x in range(9,99)]
#zone_end = ['Zone End (%s)'%x for x in range(9,99)]
#zone_time = ['Zone Time (%s)'%x for x in range(9,99)]
#zone_pretime = ['Zone Pre Time (%s)'%x for x in range(9,99)]
#cols += zone_name + zone_start + zone_end + zone_time + zone_pretime
df = data.drop(cols,axis=1)

#Filtramos por CS-CS
#df = df.drop(df[(df.Origin == 'Coya Sur') & (df.Destination ==  'Coya Sur')].index)

#Cambiamos los objetos str a fechas.
#for i in range(1,9):
#    zone_1 = "Zone Start (%s)"%i
#    zone_2 = "Zone End (%s)"%i
#    df[zone_1] = pd.to_datetime(df[zone_1])
#    df[zone_2] = pd.to_datetime(df[zone_2])

    #Guardamos el archivo
df.to_csv("BoottleNeck_filtered_v2.csv")

