import pandas as pd
import numpy as np
# esto es para consultar memoria RAM consumida..
# instalar esta librería con pip install psutil
import psutil
import os

process = psutil.Process(os.getpid())
filename = 'SQMStats_CS_Bottleneck_20190101-20210101.csv'
n_zones=98
def column_zones(_from,to):
  """
  Devuelve las 5 columnas de zona desde el index _From hasta to.
  """  
  columns = []
  for i in range(_from,to+1):
    columns.append('Zone Name (%s)' % i)
    columns.append('Zone Pre Time (%s)' % i)
    columns.append('Zone Start (%s)' % i)
    columns.append('Zone End (%s)' % i)
    columns.append('Zone Time (%s)' % i)
  return columns

# Las columnas fijas deseadas
cols_fix = ['Cycle Id','Year [In]','Month [In]','Day [In]','Hour [In]'] + ['Origin','Destination'] 
# Las columnas de zonas que se extraeran, en este caso desde (1) a (5)
cols_zones = column_zones(1,n_zones)
# Las columnas del subset de datos CSV que se extraeran
cols = cols_fix + cols_zones
# Las columnas unpivoted (no tienen el índice)
cols_unpivoted = ['Zone Name','Zone Pre Time','Zone Start','Zone End','Zone Time']
#cols_unpivoted = ['Zone Name','Zone Start','Zone Time']

def get_data(orig,dest):
  """
  Extrae un subset de datos del csv de un origen,destino y rango de zonas específico
  Lo graba en el archivo "df.csv"
  """
  df_chunk = pd.read_csv(filename, sep=',', quotechar="'", encoding='utf-8', chunksize=5000, engine='python')
  rows = []
  print("memoria inicial:",process.memory_info().rss/1000000) # in MB
  for chunk in df_chunk:
    cf = chunk[cols]
    #cf = cf[(cf['Origin']==orig) & (cf['Destination']==dest)]
    #cf = cf[(cf['Origin']==orig) & (cf['Destination']!='Coya Sur')]
    cf =cf[(cf['Origin']==orig)]
    rows.append(cf)
    #print(process.memory_info().rss/1000000)  # in MB
  df = pd.concat(rows)
  print("memoria final:",process.memory_info().rss/1000000)  # in MB 
  df.to_csv('df_CS_all_v2.csv',columns=cols,sep=',', quotechar="'", encoding='utf-8')

def unpivot(df):
  """
  Hace una operación de 'unpivot' usando la función melt (mirar pandas.melt)
  Cada fila es un ciclo de operación completo, con 5 columnas por cada zona (Name, Pre Time, Start, End, Time)
  Se busca generar una fila por cada zona, es decir, 5 filas por zona, y con nombre de columna estandar.
  Ej:
  
  Un ciclo tiene actividad en varias zonas (ZN = Zone Name, ZPT = Zone Pre Time, etc..) y VN = Valor Name, etc.)
  .....|ZN(1)|ZPT(1)|ZS(1)|ZE(1)|ZT(1)|ZN(2)|ZPT(2)|ZS(2)|ZE(2)|ZT(2)|ZN(3)|ZPT(3)|ZS(3)|ZE(3)|ZT(3)|.....
  .....|-----|------|-----|-----|-----|-----|------|-----|-----|-----|-----|------|-----|-----|-----|.....
  .....|VN(1)|VPT(1)|VS(1)|VE(1)|VT(1)|VN(2)|VPT(2)|VS(2)|VE(2)|VT(2)|VN(3)|VPT(3)|VS(3)|VE(3)|VT(3)|.....
  
  convertirlo en filas "unpivoted" (columnas sin (1), (2), etc..)
  
  .....|ZN   |ZPT   |ZS   |ZE   |ZT   |
  .....|VN(1)|VPT(1)|VS(1)|VE(1)|VT(1)|
  .....|VN(2)|VPT(2)|VS(2)|VE(2)|VT(2)|
  .....|VN(3)|VPT(3)|VS(3)|VE(3)|VT(3)|  
  
  de esta forma podemos hacer agrupaciones, filtros, etc., por fila. y con nombres fijos de columnas
  """  
  df_list=[]
  for i,var in enumerate(cols_zones):
    # son 5 columnas por zona (Zone Name,Zona Pre Time, etc..)
    j = i % 5
    var_name = cols_unpivoted[j]
    df_ = df.melt(id_vars=cols_fix,value_vars=[var],value_name=var_name)
    df_.set_index('Cycle Id')
    if j>0:
      # vamos pegando las columnas nuevas haciendo join usando como llave "Cycle Id". Eliminamos los duplicados.
      _df = _df.join(df_[['Cycle Id',var_name]],lsuffix='',rsuffix='_other').drop(['Cycle Id_other'],axis=1)
      if j==4:
        # Es la última de la zona (la columna 5) así que este dataframe se va a la lista
        df_list.append(_df)
    else:
      # la columna "variable" se genera de hacer melt, hay que eliminarla
     _df = df_.drop(['variable'],axis=1)
  # se concatenan todos los dataframes por zona
  return pd.concat(df_list)

#------------------------------------------------    
# Se genera el subset de datos (una sola vez, luego comentar esta línea)
get_data('Coya Sur','Tocopilla')
# Se lee el subset de datos
df = pd.read_csv('df_CS_all_v2.csv', sep=',', quotechar="'", encoding='utf-8', engine='python')
# Se "unpivotea"
df = unpivot(df)
# El CSV "unpivoted" lo guardamos como un CSV para auditarlo. Tendrá las columnas fijas y las unpivoteadas
df.sort_values(by=['Cycle Id']).to_csv('df_unpivoted_CS_all_v2.csv',columns=cols_fix+cols_unpivoted, sep=',', quotechar="'", encoding='utf-8')
# Para probar, ahora sí podemos calcular el tiempo promedio por Zona, Hora de cada lugar detectado..
media = df.groupby(['Zone Name', 'Hour [In]']).agg(tiempo_promedio=pd.NamedAgg(column='Zone Time', aggfunc=np.mean))
cols_media=media.columns.values.tolist()
media.to_csv('media_CS_all_V2.csv',columns=cols_media,sep=',', quotechar="'", encoding='utf-8')
print(media)
