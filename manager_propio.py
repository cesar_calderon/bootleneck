#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 10:09:11 2020

@author: cesar
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime

def block(df,date_inicial,date_final,time_inicial,time_final,condicion=0):
    """ A grandes rasgos esto es lo que hace la función block_generator parra el dataset original"""
    if condicion == 'start':
        df = df[(df['Day of year [In]'] >= date_inicial)  & (df['Day of year [In]'] <= date_final) & (df['time start'] >= time_inicial) & (df['time start'] <= time_final)]
    elif condicion == 'end':
        df = df[(df['Day of year [Out]'] >= date_inicial)  & (df['Day of year [Out]'] <= date_final) & (df['time end'] >= time_inicial) & (df['time end'] <= time_final)]
    else:
        print("ingrese una opcion valida, 'start' o 'end', se devuelve el df incial ")
    return df


def vehiculos_cola(df,date_inicial,date_final,inicio=1,fin=25,condition='end'):
    """Generamos un diccionario que contenga zona: {bloque hr: vehiculos en ella} """
    diccionario = {}
    for i in range(inicio,fin):
        start = [i-1,0,0]
        if(i == 24):
            end = [23,59,59]
        else:
            end = [i,0,0]

        time_start = datetime.time(start[0], start[1], start[2])
        time_end = datetime.time(end[0], end[1], end[2])
        df_ = block(df,date_inicial,date_final,time_start,time_end,condition)
        
        for zo in df_['ZO'].unique().tolist(): 
            block_filtered = df_[df_['ZO'] == zo]
            vehiculos_in = block_filtered.shape[0]
            if zo == 'Salida':
                continue
            else:
                if zo in diccionario.keys():
                    diccionario[zo].update({i:vehiculos_in})
                else:
                    diccionario.update({zo:{i:vehiculos_in}})
    return diccionario


def dict_to_df(dictionary):
    """ Pasamos de Dicionario a df de la forma que nosotros necesitamos"""
    
    zonas = ['Romana Salitre', 'C Secado L-4', 'C PTS', 'C NPT3', 'Romana Mto', 'C ATM', 'C NPT 1-2', 'C Prilado', 'Losa Envasado', 'C CS-4',
     'C Secado L-3', 'C Granulacion', 'Galpones', 'C NPT4', 'C CS-12', 'C CS-P.Valdivia', 'Romana 3', 'C CS-11']
    test = np.arange(1,25,1)
    count=0
    if len(dictionary.items()) == 0 :
        dictionary = {'C NPT3':{0:0}}
    for k, v in dictionary.items():
        
        a = pd.DataFrame.from_dict(v,'index')
        a = a.rename(columns={a.columns[0]:k})
        for i in test:
            if i not in a.index:
                #print("no esta este indice", i)
                a.loc[i] = 0.
        a = a.sort_index(axis=0)
        if (count == 0):
            b = a
            count +=1
            continue 
        else:
            b=pd.concat([b,a],axis=1)
    b = b.fillna(0.)
    cols_b = b.keys().tolist()
    diff = list(set(zonas) - set(cols_b))
    if (len(diff) > 0):
        for i in diff:
            b[i] = 0.
    
    b = b.reindex(sorted(b.columns), axis=1)
    return b

def analisis_por_dia(df,dia_inicial,dia_final,hora_incial=1,hora_final=25,name=None,condicion='end',metric='max'):
    "Nos entrega el análisis por una cantidad X de dias, toma los días del año"
    count = 0
    df_final = None
    for i in range(dia_inicial,dia_final+1):
        #print(i)
        aux = dict_to_df(vehiculos_cola(df,i,i,1,25,condicion))
        if count == 0:
            df_final = aux
            count += 1
        else:
            if (metric == 'max'):
                df_final = pd.concat([df_final, aux]).max(level=0)   #Aquí indico que necesito los peaks
            elif (metric == 'mean'):
                df_final = pd.concat([df_final,aux]).mean(level=0)
    if name != None:
        df_final.sort_index(axis=0).to_csv(name,index=False)
    return df_final.sort_index(axis=0)

def plot_colas_act(df1,df2,zone,title,file,yticks_l=7,yticks_u=23):
    min1 = int(df1[zone].min())
    min2 = int(df2[zone].min())
    max1 = int(df1[zone].max()+1)
    max2 = int(df2[zone].max()+1)
    
    plt.figure(figsize=(30,15))
    plt.plot(df1[zone],'bo--',lw=3,label='Cola E')
    plt.plot(df2[zone],'ro--',lw=3,label='Actividad')
    plt.title(title,fontsize=30)
    plt.xlabel("horas",fontsize=25)
    plt.ylabel("Camiones/hr",fontsize=25)
    plt.xticks(np.arange(1,25,1),fontsize=20)
    plt.yticks(np.arange(min1,max1,1),fontsize=20)
    plt.axvline(x=17,lw=5,c='g')
    plt.axvline(x=5,lw=5,c='g')

    plt.fill_between((4,6), min1, max1,color='gray', alpha=0.3)
    plt.legend(fontsize=15)
    plt.grid(True)
    plt.fill_between((16,18),min1, max1,color='gray', alpha=0.3)
    plt.savefig(file)
    plt.show()
    
def parcelar_tiempo(df_original):
    """Función que nos entrega el mismo dataframe pero con 24 columnas adicionales las cuales 
    poseen en cada una de ellas los bloques horarios y su respectiva contribución en minutos cola"""
    df = df_original.set_index(np.arange(0,df_original.shape[0],1))
    for i in range(1,25,1):
        df[i] = 0          #creo las 24 filas nuevas
    for index, row in df.iterrows():
        block_ini = row['time start'].hour
        block_fin = row['time end'].hour
        for i in range(block_ini,block_fin+1):
            # Create datetime objects for each time (a and b)
            bloque = None
            if ((i == block_ini) & (i == block_fin)):  #START cola tiene el mismo bloque que END cola
                #print(2)
                dateTimeA = datetime.datetime.combine(datetime.date.today(), row['time start'] )
                dateTimeB = datetime.datetime.combine(datetime.date.today(), row['time end'])
                dateTimeDifference = dateTimeB - dateTimeA
                #print("Entre al bloque 1, con indice ", i, " y datetimediff ", dateTimeDifference)                
            elif ((i == block_ini) & (i != block_fin)):   #Temeos cola en mas de 1 bloque pero estamos en el 1ero
                #print(1)
                aux_dif = datetime.time(i+1,0,0)
                dateTimeA = datetime.datetime.combine(datetime.date.today(), aux_dif )
                dateTimeB = datetime.datetime.combine(datetime.date.today(), row['time start'])
                dateTimeDifference = dateTimeA - dateTimeB
                #print("Entre al bloque 2, con indice ", i, " y datetimediff ", dateTimeDifference)                
            elif ((i != block_ini) & (i == block_fin)):  #cuando estamos en el ultimo bloque
                aux_dif = datetime.time(i,0,0)
                dateTimeA = datetime.datetime.combine(datetime.date.today(), aux_dif )
                dateTimeB = datetime.datetime.combine(datetime.date.today(), row['time end'])
                dateTimeDifference = dateTimeB - dateTimeA
                #print("Entre al bloque 3, con indice ", i, " y datetimediff ", dateTimeDifference) 
            elif ((i != block_ini) & (i != block_fin)): #cuando i no es igual a ninguno de los 2 bloques de inicio y fin
                hr_1 = datetime.time(1,0,0)
                hr_2 = datetime.time(2,0,0)
                dateTimeA = datetime.datetime.combine(datetime.date.today(), hr_1 )
                dateTimeB = datetime.datetime.combine(datetime.date.today(), hr_2)
                dateTimeDifference = dateTimeB - dateTimeA
                #print("Entre al bloque 4, con indice ", i, " y datetimediff ", dateTimeDifference, " con bloque fin = ", block_fin)
            

            minutos =  int(dateTimeDifference.total_seconds()/60.)
            #print(minutos)
            bloque = i+1
            df.iloc[index,bloque+13] = minutos
    return df

def stack_by_day(df,zone,dia_inicial,dia_final,metric='max',name=None,origen=None):
    "Nos entrega el análisis por una cantidad X de dias, toma los días del año"
    count = 0
    df_final = None
    if (origen != None):
        df = df[df['Origin'] == origen]
    for i in range(dia_inicial,dia_final+1):
        #print(i)
        aux,_ = convert_df_to_df(df,zone,i)
        if count == 0:
            df_final = aux
            count += 1
        else:
            if (metric == 'max'):
                df_final = pd.concat([df_final, aux]).max(level=0)   #Aquí indico que necesito los peaks
            elif (metric == 'mean'):
                df_final = pd.concat([df_final,aux]).mean(level=0)
    if name != None:
        df_final.sort_index(axis=0).to_csv(name,index=False)
    return df_final.sort_index(axis=0)

def plot_colas(df,zona,title,file=None):
    min1 = int(df['minutos/camion'].min())
    min2 = int(df['camiones'].min())
    max1 = int(df['minutos/camion'].max()+1)
    max2 = int(df['camiones'].max()+1)
    color = 'red'
    color2 = 'blue'

    fig, ax = plt.subplots(figsize=(20,10))
    plt.title(title,fontsize=25)
    plt.grid(True)

    ax.set_xlabel('Horas',fontsize=20)
    ax.set_ylabel('Min/camion',fontsize=20, color='black')
    line1 = ax.plot(df['block'], df['minutos/camion'],marker='o',linestyle='-',lw=3, color=color,label='min/truck')
    ax.tick_params(axis='y', labelcolor=color)
    ax.set_xticks(np.arange(1,25,1))
    ax.set_xticklabels(np.arange(1,25,1),fontsize=15)
    ax.set_yticks(np.arange(min1,max1,1))
    ax.set_yticklabels(np.arange(min1,max1,1),fontsize=15)

    ax2 = ax.twinx()  # instantiate a second axes that shares the same x-axis

    ax2.set_ylabel('Camiones',fontsize=20, color='black')  # we already handled the x-label with ax1
    line2 = ax2.plot(df['block'],df['camiones'],marker='o',linestyle='--',lw=3,color= color2,label='N° trucks')
    ax2.tick_params(axis='y', labelcolor=color2)
    ax2.set_yticks(np.arange(min2,max2,1))
    ax2.set_yticklabels(np.arange(min2,max2,1),fontsize=15)


    fig.tight_layout()  # otherwise the right y-label is slightly clipped

    # added these two lines
    lns = line1+line2
    labs = [l.get_label() for l in lns]
    ax.legend(lns, labs, loc=0,fontsize=20)
    if(file!=None):
        plt.savefig(file)
    plt.show()
    
def convert_df_to_df(df,zone='C NPT3',day=1):
    a = df[(df['Day of year [In]'] == day) &  (df['ZO'] == zone)] #filtramos por día y zona
    a = a.iloc[:,14:]   #nos quedamos solamente con las columnas de bloques horarios
    suma_time = a.sum().tolist()  #creamos la lista de la suma de los tiempos en cada bloque horario
    cuenta_trucks = a[a>0].count().tolist() #creamos la lista que suma los camiones que habian en ese bloque horario
    min_camion=[] #iniciamos la lista final [[x1,y1,z1],[x2,y2,z2]] donde x es el bloque, y es la suma min/camion, z es el N° de camiones
    for i in range(24):
        if (cuenta_trucks[i] == 0 or suma_time[i] == 0 ):  #no dividimos por cero
            min_camion.append([i+1,0,0])
        else:
            b = suma_time[i]/float(cuenta_trucks[i])
            min_camion.append([i+1,int(b),cuenta_trucks[i]])
    return pd.DataFrame(min_camion,columns=["block","minutos/camion","camiones"]).set_index(np.arange(1,25,1)), min_camion