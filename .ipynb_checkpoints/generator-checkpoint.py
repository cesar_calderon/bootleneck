#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 18 12:02:59 2020

@author: cesar
"""


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
import datetime
import seaborn as sns

df = pd.read_csv("BoottleNeck_filtered_v2.csv", engine='python')
df = df.drop("Unnamed: 0",axis=1)

cols = ['Year [Out]','Month [In]','Month [Out]','Day [In]','Day [Out]','Hour [In]','Hour [Out]']

#Convertimos Zone start y end en .time (hh:mm:ss)
for i in range(1,99):
    zone_1 = "Zone Start (%s)"%i
    zone_2 = "Zone End (%s)"%i
    df[zone_1] = pd.to_datetime(df[zone_1], utc = True).dt.tz_convert('Chile/Continental').dt.time
    df[zone_2] = pd.to_datetime(df[zone_2], utc = True).dt.tz_convert('Chile/Continental').dt.time
df["Date [In]"] = df['Year [In]'].astype(str) + "-" + df["Month [In]"].astype(str) + "-" + df["Day [In]"].astype(str)+" "+df['Hour [In]'].astype(str)+":00:00"
df["Date [In]"] = pd.to_datetime(df["Date [In]"],utc = True)
df["Date [Out]"] = df['Year [Out]'].astype(str) + "-" + df["Month [Out]"].astype(str) + "-" + df["Day [Out]"].astype(str)+" "+df['Hour [Out]'].astype(str)+":00:00"
df["Date [Out]"] = pd.to_datetime(df["Date [Out]"],utc = True)
#convertimos utc a Chile/Continental
df['Date [In]']=pd.to_datetime(df['Date [In]'], utc = True).dt.tz_convert('Chile/Continental')
df["Date [Out]"] = pd.to_datetime(df["Date [Out]"],utc = True).dt.tz_convert('Chile/Continental')

df['Day of year [In]']=pd.to_datetime(df['Date [In]']).dt.dayofyear
df['Day of year [Out]']=pd.to_datetime(df['Date [Out]']).dt.dayofyear

#Dejamos Date [In] como estaba
df["Date [In]"] = pd.to_datetime(df["Date [In]"]).dt.date
df["Date [Out]"] = pd.to_datetime(df["Date [Out]"]).dt.date
df=df.drop(cols,axis=1)
df



zonas = ['Romana Salitre', 'C Secado L-4', 'C PTS', 'C NPT3', 'Romana Mto', 'C ATM', 'C NPT 1-2', 'C Prilado', 'Losa Envasado', 'C CS-4',
 'C Secado L-3', 'C Granulacion', 'Galpones', 'C NPT4', 'C CS-12', 'C CS-P.Valdivia', 'Romana 3', 'C CS-11']
def block_generator(dia_inicio,dia_fin,inicio,fin,condition='in'):
    """Entregamos un Año, mes, día  y el rango de horas entre las cuales se quiere tener el bloque, 
       hr:min:seg > block >hr_:min_:seg_ """
    cont = 0
    for i in range(1,99):
        zone_1 = "Zone Name (%s)"%i
        zone_2 = "Zone Pre Time (%s)"%i
        zone_3 = "Zone Start (%s)"%i
        zone_4 = "Zone End (%s)"%i
        zone_5 = "Zone Time (%s)"%i

        cols=['Cycle Id', 'Origin', 'Destination','Date [In]','Date [Out]','Day of year [In]','Day of year [Out]',
              zone_1, zone_2, zone_3, zone_4, zone_5]

        date_lower = datetime.time(inicio[0], inicio[1], inicio[2])
        date_upper = datetime.time(fin[0], fin[1], fin[2])
        
        if (condition == 'in'):
            a = df[(df[zone_3] < date_upper) & (df[zone_3] >= date_lower) & (df['Day of year [In]'] >= dia_inicio) & (df['Day of year [In]'] <= dia_fin)] 
        elif (condition == 'out'):
            a = df[(df[zone_4] < date_upper) & (df[zone_4] >= date_lower) & (df['Day of year [Out]'] >= dia_inicio) & (df['Day of year [Out]'] <= dia_fin)]     
        else:
            break
        b = a.loc[:,cols]
        b = b.rename(columns={zone_1:"Name ZO",
                          zone_2:"Pre Time",
                          zone_3:"Start",
                          zone_4:"End",
                          zone_5:"Time"})    
        if cont == 0:
            block = b
            cont += 1
            continue
        else:
            block = pd.concat([block, b], axis=0, sort=False) 
    block = block.sort_values(['Cycle Id', 'Name ZO',"Date [In]"], ascending=[True,True,True])
    return block

#Vehiculos ingresados
def vehiculos(dia_inicio,dia_fin,inicio=1,fin=25,condition='in'):
    """Generamos un diccionario que contenga zona: {bloque hr: vehiculos ingresados} """
    diccionario = {}
    for i in range(inicio,fin):
        start = [i-1,0,0]
        if(i == 24):
            end = [23,59,59]
        else:
            end = [i,0,0]
        if (condition == 'out'):
            df_ = block_generator(dia_inicio,dia_fin,start,end,condition=condition)
            date_lower = datetime.time(start[0], start[1], start[2])
            date_upper = datetime.time(end[0], end[1], end[2])
        elif (condition == 'in'):
            df_ = block_generator(dia_inicio,dia_fin,start,end,condition=condition)
        else:
            break
        for zo in df_['Name ZO'].unique().tolist(): 
            block_filtered = df_[df_['Name ZO'] == zo]
            if (condition == 'out'):
                block_filtered = block_filtered[(block_filtered['End'] < date_upper) & (block_filtered['End'] >= date_lower)]
            vehiculos_in = block_filtered.shape[0]
            if zo == 'Salida':
                continue
            else:
                if zo in diccionario.keys():
                    diccionario[zo].update({i:vehiculos_in})
                else:
                    diccionario.update({zo:{i:vehiculos_in}})
    return diccionario

def vehiculos_time(dia_inicio,dia_fin,inicio=1,fin=25,condition='in',cond='time'):
    """Generamos un diccionario que contenga zona: {bloque hr: vehiculos salieron} """
    diccionario = {}
    for i in range(inicio,fin):
        start = [i-1,0,0]
        if(i == 24):
            end = [23,59,59]
        else:
            end = [i,0,0]
        df_ = block_generator(dia_inicio,dia_fin,start,end,condition=condition)
        date_lower = datetime.time(start[0], start[1], start[2])
        date_upper = datetime.time(end[0], end[1], end[2])
        for zo in df_['Name ZO'].unique().tolist(): 
            block_filtered = df_[df_['Name ZO'] == zo]
            block_filtered = block_filtered[(block_filtered['End'] < date_upper) & (block_filtered['End'] >= date_lower)]
            if(cond=='pre time'):
                cumulative_time = block_filtered['Pre Time'].sum()
            else:
                cumulative_time = block_filtered.Time.sum()
            if zo == 'Salida':
                continue
            else:
                if zo in diccionario.keys():
                    diccionario[zo].update({i:(cumulative_time/60)})
                else:
                     diccionario.update({zo:{i:(cumulative_time/60)}})                
    return diccionario

def dict_to_df(dictionary):
    """ Pasamos de Dicionario a df de la forma que nosotros necesitamos"""
    count=0
    for k, v in dictionary.items():
        a = pd.DataFrame.from_dict(v,'index')
        a = a.rename(columns={a.columns[0]:k})
        if (count == 0):
            b = a
            count +=1
            continue 
        else:
            b=pd.concat([b,a],axis=1)
    b = b.fillna(0)
    cols_b = b.keys().tolist()
    diff = list(set(zonas) - set(cols_b))
    #print(b)
    #print(diff)
    if (len(diff) > 0):
        for i in diff:
            #print(i)
            b[i] = 0
    b = b.reindex(sorted(b.columns), axis=1)
    #print(b)
    return b




dias_mes = [[1,31],[32,60],[61,91],[92,121],[122,152],[153,160],[182,212],[213,243],[244,273],[274,304],[305,334],[335,365]]
#Enero
count = 0
M_jan = None
month = ["Abril", "Mayo","Junio"]
x=3

for j in month:
    for i in range(dias_mes[x][0],dias_mes[x][1]+1):
        print(i)
        aux = dict_to_df(vehiculos(i,i,1,25,condition="out"))
        if count == 0:
            M_jan = aux
            count += 1
        else:
            M_jan = pd.concat([M_jan, aux]).max(level=0)
    count = 0
    M_jan.to_csv(j+"max_out.csv")
    x+=1
